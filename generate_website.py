#!/usr/bin/env python

'''
Markdown website generator
A simple Markdown-to-HTML website generator, based on Markdown files and Git.

Copyright (C) 2017  Andre Castro
License: [GPL3](https://www.gnu.org/licenses/gpl-3.0.html) 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

andre@andrecastro.info
http://oooooooooo.io/
'''


import os,  shlex, subprocess, re, yaml
from jinja2 import Template, Environment, FileSystemLoader
from pprint import pprint
from argparse import ArgumentParser

# Arguments        
p = ArgumentParser()
p.add_argument("--local", action='store_true', help="use --local when running the script on local machine")
p.add_argument("--server", action='store', help="use --remote to indicate the path to the website dir, when running the script on server")

args = p.parse_args()

content_path = './pages'

if args.local is True:
    wd = '.'
    os.chdir(wd)                
else:
    wd = args.server
    os.chdir(wd)            


# DEFs
def pandoc2html(md_file):
    # convert md files to html
    args_pandoc = shlex.split( 'pandoc -f markdown -t html5 {infile}'.format(infile=md_file) )
    pandoc = subprocess.Popen(args_pandoc, stdout=subprocess.PIPE)
    html = pandoc.communicate()[0]
    html = html.decode("utf-8")
    return html


def parse_filetree(path):
    # given a path the funct creates a dictionary of all .md in the `path` dir and sub dirs.
    filetree = {}
    for (dirpath, dirs, files) in os.walk(path):        
        for f in files:
            filepath =  (os.path.join(dirpath, f))
            if '.md' in filepath:
                item_dir_fullpath = os.path.abspath(filepath)
                filetree[filepath] = {} # directory content
                dirpath = dirpath#.replace('./pages','.')
                #print dirpath, filepath
                keys = ['file_path','title','author', 'date' ] # file info
                filetree[filepath]={}
                filetree[filepath]={ key:None for key in keys }
                filetree[filepath]['file_path'] = (filepath.replace('./pages/',''))
                filetree[filepath]['title'] = ( os.path.split(filepath)[-1]).replace('.md','')
    #pprint(filetree[filepath])
    return filetree



def generate_menu(site_dict): 
    # creates the side bar menu.
    # for key in site_dict.keys():
    #     if 'category' not in site_dict[key].keys():
    #         print key, site_dict[key].keys()
    
    key_date=[(key,site_dict[key]['date'],(site_dict[key]['category'].lower())) for key in site_dict.keys()] #also including category as 3rd item
    key_date_sorted = sorted(key_date, key=lambda tup: tup[1], reverse=True)
    dirs_all = (list(set([ os.path.split(os.path.split(key)[0])[-1] for key in site_dict.keys()])))
    # dirs_all is dirs in template = ['me', 'blog', 'works', 'cookbook'] # list of dir :

#    pprint(site_dict)

    # loop through dict for creating child li
    menu=Template('''
<ul class="menu_sections">

{% for item_dir in dirs %} {# loop through dirs_all to create menu sections=li #}

    <li class="menu_sections">{{ item_dir|capitalize }}
    <ul class="menu_items">

    {% for i in file_date_tuples %}
        {% set date=i[1] %}
        {% set key=i[0] %}
        {% set category=i[2] %}
        {% set item_path = site_dict[key]['file_path'] %}
        {% set item_date = site_dict[key]['date'] %}
        {% set item_title = site_dict[key]['title'] %}

        {% if item_dir == category  %} {# both need to lower #}
        <li class="menu_items">

            {% if item_date != 'None' and item_date != None  %}
                {{item_date|replace('_','.')}} 
            {% endif %}

            <a href="../{{item_path|replace('.md','.html')}}">{{item_title}}</a>
            </li>
        {% endif %}

    {% endfor %}
    </ul>
{% endfor %}
</ul>
''')

    menu_rendered = menu.render(dirs=dirs_all, site_dict=site_dict, file_date_tuples=key_date_sorted)
    return menu_rendered


re_yamlblock = re.compile(r'^-{2,5}\n((.*?\n)*?)\.{2,5}', flags=re.MULTILINE)

def parse_yaml(filename):
    filename = filename #'2007-Works-BoatPeople.md'
    file_read=open(filename, 'r');
    txt=file_read.read()
    try:
        yamlblock = re_yamlblock.findall(txt )[0][0]
        yamldict = yaml.load(yamlblock)
        return yamldict
    except:
       return None

def dict_add_yaml(dic):
    # dic (receive and returned) will be updated with yaml metadata
    for file_path, file_metadata  in dic.iteritems(): # loop through each file entry
        yaml_dict = parse_yaml(file_path) #search for yaml metadata in file 
#        pprint(yaml_dict)

        if yaml_dict: # if metadata found: update dic with it 
            for yaml_item_key, yaml_item_val  in yaml_dict.iteritems(): #each metadata item                
                if yaml_item_val is not None:
                    if yaml_item_key == 'date' and type(yaml_item_val) is int:
                        yaml_item_val = str(yaml_item_val) #make date str

                    if yaml_item_key == 'date' and yaml_item_val.isdigit() is True:
                        if len(yaml_item_val) == 8: #yyyymmdd
                            yyyy_mm_dd="{}_{}_{}".format(yaml_item_val[:4],yaml_item_val[4:6],yaml_item_val[6:])
                            #print 'yyyymmdd', yaml_item_val,  yyyy_mm_dd
                            yaml_item_val =  yyyy_mm_dd
                        
                dic[file_path][yaml_item_key]=yaml_item_val
                #print dic[file_path], yaml_item_key, dic[file_path][yaml_item_key]
    return dic
    
def generate_html_pages(site_dict):
    # based on site_dict
    site_dict = dict_add_yaml(site_dict) # add metadata to each site_dict entry

    for key in site_dict.keys(): # check all necessary metadata is present to include document in website 
        metadata_vals = [site_dict[key]['date'], site_dict[key]['category'], site_dict[key]['title']]
        #print metadata_vals
        if None in metadata_vals:
            print 'MISSING METADATA: {} from {}'.format( metadata_vals,key)
            site_dict.pop(key) #remove entry from dict

    #pprint(site_dict)
        
    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template('template_base.html')
    site_menu = generate_menu(site_dict)

    for file_path, file_metadata  in site_dict.iteritems():        
        item_dir_fullpath = os.path.abspath(file_path)
        md_file = file_path
        html_content = pandoc2html(md_file)
        output_from_parsed_template = template.render(menu=site_menu, content=html_content, title=file_metadata['title'])             
        html_file=md_file.replace('.md','.html')
        html_file_open = open(html_file, 'w')
        html_file_open.write( (output_from_parsed_template).encode('utf-8'))
        html_file_open.close()
        #print  md_file, '--->', html_file        

# ACTION        
site_dict = parse_filetree(content_path)
generate_html_pages( site_dict )
print  '** Website generated **'
#pprint(site_dict)
