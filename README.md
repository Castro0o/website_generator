# Markdown website generator

A simple Markdown-to-HTML website generator, based on Markdown files and Git.

Its central idea is to spare editors from having to done with any conversions,  scripts, or HTML. Simply edit the markdown files, `git add -`, `git commit -m "my edits"`, `git push`. A git's post-receive-hook in the website's server will trigger the conversations and scripts which generate the website from the markdown files. Locally there are no dependencies, except Git. 

## For Editors

### Important files and folders:
* `pages/` contains
  	   * sub-folders: which result in website sections
  	   * .md (Markdown) files: which result in website (.html) pages
* `media/` contains the website media: images, videos, and audio files 
* `lib/` contains .css and .js files

### How to:

**Create or edit an page:**
* create/edit **only** Markdown files (.md) inside the sub-folders from the `pages` folder.
* Use plain text editor for editing: e.g. [Sublime Text](http://sublimetext.com/), [gedit](https://wiki.gnome.org/Apps/Gedit), [Brackets](http://brackets.io/), etc. Word-processors such as Word or Mac text editor are *unsuitable* as they will add formatting information to the text, which we don't want. All formatting should be done explicitly using Markdown (and/or HTML)!
* Avoid using spaces in file names.

**Add metadata\:**
The website generator requires the inclusion, in each page, of a metadata block. Add it, by using the following YAML example block at the top of each page:

	  ---
	  category: Blog
	  date: '20160816'
	  title: Permutations1
	  ...

* category: is usually the same name as the folder the page is in
* date: in the form `yyyymmdd`
* title: title of the page


**Add images:**
* store your image in the folder `media/`
* include it in an article by using the markdown syntax for images: `![img caption](img location)`
* the image location will always begin with `media/`, follow by the image name
* i.e. `![Troll](../../media/troll.png)`


**Publish**
From within this folder run `git add --all` to add all new or modified files

Create a commit `git commit --message="describing what change in this commit"`
Push commit to repository (AKA publish) `git push origin` origin will be the git the server that stores the website and performs the necessary conversions.

## For Designers \& Developers

### Dependencies: 
* Python 2.7
* Python external libraries :
    * [Jinja](http://jinja.pocoo.org/) templating engine
	* [PyYAML](https://pypi.python.org/pypi/PyYAML/3.12)
* [Pandoc](http://pandoc.org/)
* Git


### Git Hooks 
A Git post-receive hook is used to run the `generate_website.py`, when a git commit is pushed to server.

In order to get the `post-receive-hook` to perform that task, a few steps need to be taken: 


#### Steps (NEEDS EDITING)
**Locally**:
* clone this repository `git clone https://Castro0o@gitlab.com/Castro0o/website_generator.git`
* Go to `website_generator.git` directory: `cd website_generator.git`
* Remove git folder and .gitignore folder: `rm -rf .git .gitignore`
* initiate a new repository: `git init`
* add all files (including placeholder content files inside pages/ which later might be changed or remove): `git add -A`

**Server side:**

* create a git bare repository:
   * create directory for remote `mkdir mywebsite.git` 
   * go inside that directory `cd mywebsite.git` 
   * create a git bare repository `git --bare init`

* create directory for your website (website dir): `mkdir ~/public_html/mywesite.sexy/`


**Locally:**
* edit, add, commit, and push the post-receive-hook script:
    * change the `git_work_tree_path` variable to the full path of the website directory in the server. i.e `git_work_tree_path=/home/user/public_html/rms.sexy` 
	* `git add post-receive-hook`
	* `git commit -m "post-receive-hook edited to match my website dir"`
	* add the server-side git bare repository as a git remote: `git remote add remotename ssh://username@server-address:/path/to/mywebsite.git`
	* push: `git push remotename master`

* copy the post-receive-hook to the git bare repository `scp post-receive-hook username@server-address:/path/to/mywebsite.git`

**Server side:**
On the git bare repository:

* mv the post-receive-hook to the git bare repository's `hooks/post-receive` file: `mv post-receive-hook hooks/post-receive`
* `chmod +x hook/post-receive`
* test it by running `./hooks/post-receive`
* Files from the git bare repository will be checkout to the `$git_work_tree_path` 
* Errors due to non-installed python modules will be printed. Take the opportunity to install them. And test again.
* When you manage to run it without errors, the files from the bare repository should be the in website dir (`$git_work_tree_path`), inside the pages directory, including the .html files created by generate_website.py


**Locally:**

* edit any content .md files
* `git add -u`
* `git commit -m "changes content"` 
* push to remote: `git push remotename master`

If all goes well the .md your changes will be received by the server, copied to the website dir and converted to html files. Et voila, your website! To see it use the browser and go to the `pages/` folder.



### Run conversion scripts locally
It is likely that you'd like to see what the website looks like, locally, do so:
* run `python generate_website.py --local`
* browse the .html files inside the `pages/` sub-folders 


### Development & Customization

#### look and behavior
If you want to change the look and behavior of the website, the director `lib/` has the files you need
* `fonts/`
* `interaction.js`
* `jquery-3.1.1.min.js`
* `style.css`

#### website generator 
If you want to change the behavior of the website generator the essential files are:
* `generate_website.py`: converts .md files onto .html pages, using the `templates/template_base.html`.
   * also contains the template for the menu
* `templates/template_base.html` the template used by Jinja in `generate_website.py`.

Run locally: `python generate_website.py --local`




[Andre Castro](http://oooooooooo.io/) 2017
